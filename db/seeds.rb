# encoding: utf-8
#
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

PersonCategory.delete_all
PersonCategory.create(id: 1, name: "Funcionário")
PersonCategory.create(id: 2, name: "Cliente")
PersonCategory.create(id: 3, name: "Outros")
PersonCategory.create(id: 4, name: "Fornecedor")
PersonCategory.create(id: 5, name: "Advogado")
PersonCategory.create(id: 6, name: "Fisco")