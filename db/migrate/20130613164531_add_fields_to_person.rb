class AddFieldsToPerson < ActiveRecord::Migration
  def change
    add_column :people, :state_register, :string
    add_column :people, :municipal_register, :string
    add_column :people, :enrollment, :string
    add_column :people, :activity_area, :string
    add_column :people, :register, :string
    add_column :people, :voter_register, :string
    add_column :people, :cpf_cnpj, :string
  end
end
