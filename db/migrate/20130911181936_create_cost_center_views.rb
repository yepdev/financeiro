class CreateCostCenterViews < ActiveRecord::Migration
  def change
    create_table :cost_center_views do |t|
      t.integer :admin_user_id
      t.integer :cost_center_id

      t.timestamps
    end
    add_index :cost_center_views, :admin_user_id
    add_index :cost_center_views, :cost_center_id
  end
end
