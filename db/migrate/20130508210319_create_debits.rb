class CreateDebits < ActiveRecord::Migration
  def change
    create_table :debits do |t|
      t.string :description
      t.decimal :amount
      t.date :due_date
      t.string :status
      t.integer :core_id
      t.integer :cost_center_id
      t.integer :person_id

      t.timestamps
    end
  end
end
