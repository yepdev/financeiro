class CreatePersonCategories < ActiveRecord::Migration
  def change
    create_table :person_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
