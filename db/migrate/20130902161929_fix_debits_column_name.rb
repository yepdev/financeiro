class FixDebitsColumnName < ActiveRecord::Migration
  def up
  	rename_column :debits, :type, :debit_type
  end

  def down
  	rename_column :debits, :debit_type, :type
  end
end
