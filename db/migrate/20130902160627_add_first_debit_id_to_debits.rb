class AddFirstDebitIdToDebits < ActiveRecord::Migration
  def change
    add_column :debits, :first_debit_id, :integer
  end
end
