class RemoveClientIdFromDebit < ActiveRecord::Migration
  def up
    remove_column :debits, :client_id
  end

  def down
    add_column :debits, :client_id, :integer
  end
end
