class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.decimal :amount
      t.integer :core_id
      t.integer :cost_center_id
      t.string :description
      t.date :due_date
      t.integer :person_id
      t.integer :client_id
      t.string :status

      t.timestamps
    end
  end
end
