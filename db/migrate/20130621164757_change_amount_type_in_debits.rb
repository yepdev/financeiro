class ChangeAmountTypeInDebits < ActiveRecord::Migration
  def up
  	change_column :debits, :amount, :float
  end

  def down
  	change_column :debits, :amount, :decimal
  end
end
