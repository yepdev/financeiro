class CreateUploadFiles < ActiveRecord::Migration
  def change
    create_table :upload_files do |t|
      t.string :file_ref
      t.integer :debit_id
      t.integer :income_id

      t.timestamps
    end
  end
end
