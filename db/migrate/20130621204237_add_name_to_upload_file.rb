class AddNameToUploadFile < ActiveRecord::Migration
  def change
    add_column :upload_files, :name, :string
  end
end
