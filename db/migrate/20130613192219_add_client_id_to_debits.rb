class AddClientIdToDebits < ActiveRecord::Migration
  def change
    add_column :debits, :client_id, :integer
  end
end
