class AddTypeToDebits < ActiveRecord::Migration
  def change
    add_column :debits, :type, :string
  end
end
