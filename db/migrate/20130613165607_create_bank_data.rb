class CreateBankData < ActiveRecord::Migration
  def change
    create_table :bank_data do |t|
      t.integer :person_id
      t.integer :bank_number
      t.string :agency
      t.string :account_number
      t.string :operation_code

      t.timestamps
    end
  end
end
