class AddClientToDebit < ActiveRecord::Migration
  def change
  	add_column :debits, :client_id, :integer
  	add_index :debits, :client_id
  end
end
