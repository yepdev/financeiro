class ChangeAmountTypeInIncomes < ActiveRecord::Migration
  def up
  	change_column :incomes, :amount, :float
  end

  def down
  	change_column :incomes, :amount, :decimal
  end
end
