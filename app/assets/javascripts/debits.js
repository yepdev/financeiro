$(document).ready(function() {
	$('#debit_amount').maskMoney({showSymbol:true, symbol:"R$ ",thousands:".", symbolStay: true, decimal: ","});
	
	if ($('#debit_amount').length > 0) {
		// on submit, return the right value to ammount
		$('form').on('submit', function() {
			if ($('#debit_amount').val() != "") {
				right_value = $('#debit_amount').val()
				right_value = right_value.replace("R$ ", "");

				while (right_value.indexOf(".") != -1) {
					right_value = right_value.replace(".", "")
				}

				right_value = right_value.replace(",", ".")
				$('#debit_amount').val(right_value);
			}
		});
		
		if ($('#debit_amount').val() != "") {
			float_val = $('#debit_amount').val()
			float_val = parseFloat(float_val).toFixed(2)
			$('#debit_amount').val(float_val);
			$('#debit_amount').maskMoney('mask')
		}
	}


	if ($("#debit_debit_type_parcelado") != undefined && $("#debit_debit_type_parcelado").length > 0) {
		$("#debit_debit_type_parcelado").on('click', function() {
			clearDebitoRecorrente();

			if ($('.num_parcelas').length == 0) {
				append = "<p class=\"num_parcelas\"><span>Número de Parcelas: </span><input id=\"parcelas_num\" name=\"parcelas_numero\" value=\"1\" type=\"number\" style=\"width: 60px;\"/></p>"
				$('label[for="debit_debit_type_parcelado"]').parent().append(append);

				$('#parcelas_num').on("change", function() {
					if ($('input[name="debit[debit_type]"]:checked').val() == "parcelado") {
						valor = parseInt($('#parcelas_num').val())

						if (!isNaN(valor)) {
							vencimento_1 = $('#debit_due_date_input');
							vencimento_1.addClass("vencimento");
							vencimento_1.find('label').html("Vencimento - Parcela 1");

							vencimentos_existentes = $(".vencimento");

							if (vencimentos_existentes.length > valor) {
								$('.vencimento[id!="debit_due_date_input"]').remove();
							}

							while (valor > $(".vencimento").length) {
								clone = vencimento_1.clone();
								clone.attr("id", clone.attr("id") + "_" + $(".vencimento").length);
								clone.find("input").attr("name", "data_vencimento_" + $(".vencimento").length);
								clone.find("input").attr("id", "data_vencimento_" + $(".vencimento").length);

								clone.find('label').html("Vencimento - Parcela " + ($(".vencimento").length + 1))

								clone.insertAfter($(".vencimento").last());
							}
						}
					}
				})
			}
		});
	}

	if ($("#debit_debit_type_recorrente") != undefined && $("#debit_debit_type_recorrente").length > 0) {
		$("#debit_debit_type_recorrente").on('click', function() {
			clearDebitoParcelas();

			if ($('.recorrente').length == 0) {
				append = "<p class=\"recorrente\"><span>Número de Meses do contrato: </span><input id=\"recorrente_meses\" name=\"recorrente_meses\" value=\"1\" type=\"number\" style=\"width: 60px;\"/></p>"
				$('label[for="debit_debit_type_recorrente"]').parent().append(append);

			}
		});
	}

	if ($("#debit_debit_type_fixo") != undefined && $("#debit_debit_type_fixo").length > 0) {
		$("#debit_debit_type_fixo").on('click', function() {
			clearDebitoParcelas();
			clearDebitoRecorrente();
		});
	}

	$('#debit_person_id').on('change', function() {
		person_id = $(this).val();

		if (person_id != undefined && person_id != "") {
			productionEnvironment = $('div.rails-env').attr('data-env') == "production";
			$.ajax({
				url: (productionEnvironment ? "/financeiro" : "") + "/admin/debitos/last_data",
				data: {person_id: person_id},
				success: function(data) {
					console.log(data)
					console.log(data.amount)
					if (data.client_id != undefined) {
						$('#debit_client_id').val(data.client_id)
						$('#debit_client_id').trigger("liszt:updated");
					}

					if (data.cost_center_id != undefined) {
						$('#debit_cost_center_id').val(data.cost_center_id)
						$('#debit_cost_center_id').trigger('liszt:updated');
					}

					if (data.amount != undefined) {
						$('#debit_amount').val(parseFloat(data.amount).toFixed(2))
						$('#debit_amount').maskMoney('mask')
					}

					if (data.description != undefined) {
						$('#debit_description').val(data.description);
					}	
				}
			});
		}
	});
});

function clearDebitoParcelas() {
	$('.num_parcelas').remove();
	$('.vencimento[id!="debit_due_date_input"]').remove();
	$('#debit_due_date_input').find("label").html("Vencimento");
}

function clearDebitoRecorrente() {
	$('.recorrente').remove();
}