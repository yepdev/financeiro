//= require active_admin/base
//= require jquery_nested_form
//= require_tree .

$(document).ready(function() {

	//FIXME pog extrema para burlar o problema de suburi em produção
	var $links = $('#header #tabs').find('a');
	$links.each(function(){
		console.log($(this).attr('href'));
		$(this).attr('href','/financeiro' + $(this).attr('href'));
		console.log($(this).attr('href'));
	})

	$('#debito_due_date').datepicker({dateFormat:"dd/mm/yy"});
	$('#receita_due_date').datepicker({dateFormat:"dd/mm/yy"});
	
	$('.clear_filters_btn').on('click', function(evt) {
		evt.preventDefault();

		// form
		var $form = $(this).parent().parent()

		$form.find('input[type!="submit"]').each(function(i, el) {
			$(this).val("");
		});

		$form.find('select').each(function(i, el) {
			$(this).val("");
		});

		$form.find('input[type="submit"]').click();
	});
});
$(document).on('nested:fieldAdded',function(event){
  // this field was just inserted into your form
  var field = event.field;
  // it's a jQuery object already! Now you can find date input
  var dateField = field.find('.datepicker');
  // and activate datepicker on it
  dateField.datepicker({dateFormat:"dd/mm/yy"});

})
