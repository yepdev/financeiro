$(document).ready(function(){

    if ($("#person_cpf_cnpj") != undefined && $("#person_cpf_cnpj").length > 0) {
    	if ($("#person_cpf_cnpj").val().length == 18) {
    		$("#person_cpf_cnpj").mask("99.999.999/9999-99")
    		$('#cnpj_radio').prop('checked', true)
    	} else {
        	$("#person_cpf_cnpj").mask("999.999.999-99")
        }

        $('#cpf_radio').click(function(){
            $("#person_cpf_cnpj").mask("999.999.999-99")
        });

        $('#cnpj_radio').click(function(){
            $("#person_cpf_cnpj").mask("99.999.999/9999-99")
        });
    }

});