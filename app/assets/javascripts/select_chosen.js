$(document).ready(function() {

 	var chosen_props = {
		no_results_text: "Nenhum resultado encontrado",
		allow_single_deselect: true,
		placeholder_text: "Selecione..."
	};
	$(".chzn-with-drop ").chosen(chosen_props);

	// $('select').chosen(chosen_props);

	$(document).on('nested:fieldAdded',function(event){
	  // this field was just inserted into your form
	  var field = event.field;
	  // it's a jQuery object already! Now you can find date input
	  var select = field.find('select.chosen');
	  // and activate datepicker on it
	  select.chosen(chosen_props);
	})

	$('select').each(function(i, el) {
		$(this).chosen(chosen_props);
	});

});
