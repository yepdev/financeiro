$(document).ready(function() {
	$('#receita_amount').maskMoney({showSymbol:true, symbol:"R$ ",thousands:".", symbolStay: true, decimal: ","});
	
	if ($('#receita_amount').length > 0) {
		// on submit, return the right value to ammount
		$('form').on('submit', function() {
			if ($('#receita_amount').val() != "") {
				right_value = $('#receita_amount').val()
				right_value = right_value.replace("R$ ", "");

				while (right_value.indexOf(".") != -1) {
					right_value = right_value.replace(".", "")
				}

				right_value = right_value.replace(",", ".")
				$('#receita_amount').val(right_value);
			}
		});
		
		if ($('#receita_amount').val() != "") {
			float_val = $('#receita_amount').val()
			float_val = parseFloat(float_val).toFixed(2)
			$('#receita_amount').val(float_val);
			$('#receita_amount').maskMoney('mask')
		}
	}

	$('#income_person_id').on('change', function() {
		person_id = $(this).val();

		if (person_id != undefined && person_id != "") {
			$.ajax({
				url: "/financeiro/admin/receita/last_data",
				data: {person_id: person_id},
				success: function(data) {
					console.log(data)
					console.log(data.amount)
					if (data.client_id != undefined) {
						$('#income_client_id').val(data.client_id)
						$('#income_client_id').trigger("liszt:updated");
					}

					if (data.cost_center_id != undefined) {
						$('#income_cost_center_id').val(data.cost_center_id)
						$('#income_cost_center_id').trigger('liszt:updated');
					}

					if (data.amount != undefined) {
						$('#income_amount').val(parseFloat(data.amount).toFixed(2))
						$('#income_amount').maskMoney('mask')
					}		


					if (data.description != undefined) {
						$('#income_description').val(data.description);
					}			
				}
			});
		}
	});
});