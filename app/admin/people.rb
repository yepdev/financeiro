# encoding: utf-8
ActiveAdmin.register Person, as: "Pessoa" do

  menu label: "Pessoas", parent: "Cadastros", priority: 3

  index do 
  	column "COD" do |pessoa|
  		pessoa.id
  	end
  	column "Nome" do |pessoa|
  		pessoa.name
  	end
  	column "CPF/CNPJ" do |pessoa|
  		pessoa.cpf_cnpj
  	end
  	column "E-mail" do |pessoa|
  		pessoa.email
  	end
  	column "Empresa" do |pessoa|
  		pessoa.company
  	end
  	column do |pessoa|
		link_to("Detalhes", admin_pessoa_path(pessoa)) + " | " + \
		link_to("Editar", edit_admin_pessoa_path(pessoa)) + " | " + \
		link_to("Remover", admin_pessoa_path(pessoa), :method => :delete, :confirm => "Tem certeza que deseja excluir?")
  	end
  end

  form :partial => "form"
 #  form do |f|
 #  	f.inputs "Dados" do
 #  		f.input :name
 #      f.input :person_category, as: :select, label: "Categoria"
 #  		f.input :register
 #      f.input :cpf_radio, type: :radio
 #  		f.input :cpf_cnpj
 #  		f.input :company
 #  		f.input :email
 #  		f.input :phone
 #  		f.input :cellphone
 #  		f.input :state_register
 #  		f.input :municipal_register
 #  		f.input :enrollment
 #  		f.input :activity_area
 #  		f.input :voter_register
 #  		f.input :addrress
 #  	end

	# f.inputs "Dados Bancários", :for => [:bank_data, f.object.bank_data || BankData.new] do |j|
	# 	j.input :bank_number
	# 	j.input :agency
	# 	j.input :account_number
	# 	j.input :operation_code
	# end

 #  	f.buttons
 #  end
end