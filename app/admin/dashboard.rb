# encoding: UTF-8
ActiveAdmin.register_page "Dashboard" do

  action_item do
      link_to "Cadastrar Despesa", new_admin_debito_path()
  end

  action_item do
      link_to "Cadastrar Pessoa", new_admin_pessoa_path()
  end

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }
  content :title => proc{ I18n.t("active_admin.dashboard") } do

    columns do

     column do
          panel "Próximos Débitos" do
              table_for Debit.where('status = ? OR status = ?', Debit::STATUS_PENDENTE, Debit::STATUS_CONFERIDO).order('id desc').limit(10) do
                  column("Cod")   {|debito| debito.id}
                  column("Status")   {|debito| status_tag debito.status, debito.status_tag                             }
                  column("Pessoa"){|debito| link_to(debito.person.name, admin_pessoa_path(debito.person)) unless debito.person.nil? }
                  column("Total")   {|debito| number_to_currency debito.amount                       }
              end
          end
      end

      if current_admin_user.admin
        column do
            panel "Próximas Receitas" do
               table_for Income.where('status = ? OR status = ?', Income::STATUS_A_EMITIR, Income::STATUS_A_RECEBER).order('id desc').limit(10) do
                    column("Cod")   {|receita| receita.id}
                    column("Status")   {|receita| status_tag receita.status, receita.status_tag                             }
                    column("Pessoa"){|receita| link_to(receita.person.name, admin_pessoa_path(receita.person)) unless receita.person.nil? }
                    column("Total")   {|receita| number_to_currency receita.amount                       }
                end
            end
        end
      end

    end

    columns do

      column do
          panel "Débitos" do
            div do
              br
              #FIXME, url using url_for
              text_node %{<iframe src="charts/debits" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
            end
          end
      end

      if current_admin_user.admin
        column do
            panel "Receitas" do
              div do
                br
                text_node %{<iframe src="charts/incomes" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
              end
            end
        end
      end
    end
  end # content
end
