ActiveAdmin.register CostCenter, as: "Centros De Custo" do
  menu label: "Centros de Custo", parent: "Cadastros", priority: 1

  index do
	  	column "COD" do |cost_center|
	      cost_center.id
	    end
	    column "Nome" do |cost_center|
	      cost_center.name
	    end
	    
	    column do |cost_center|
	      link_to("Detalhes", admin_centros_de_custo_path(cost_center)) + " | " + \
	      link_to("Editar", edit_admin_centros_de_custo_path(cost_center)) + " | " + \
	      link_to("Remover", admin_centros_de_custo_path(cost_center), :method => :delete, :confirm => "Tem certeza que deseja excluir?")
	    end

  	end
  
end
