# encoding: UTF-8
ActiveAdmin.register Debit, as: "Debito" do
  menu label: "Contas a Pagar"


  collection_action :last_data, :method => :get do
    debito = Debit.where("person_id = ?", params[:person_id]).order("id DESC").limit(1)

    render json: debito.first.to_json
  end

  before_filter :only => [:index] do
    if params['commit'].blank? and params['q'].blank?
      # filtro do mes
       params['q'] = {:due_date_gte => Date.today.at_beginning_of_week.strftime("%Y-%m-%d"), :due_date_lte => Date.today.at_end_of_week.strftime("%Y-%m-%d")} 
    end
  end

  controller do
    def next_status
      @debit = Debit.find(params[:id])
      @debit.next_status

      redirect_to :action => :index, :notice => "Status alterado com sucesso."
    end

    def previous_status
      @debit = Debit.find(params[:id])
      @debit.previous_status

      redirect_to :action => :index, :notice => "Status alterado com sucesso."
    end

    def create
      @debito = Debit.new(params[:debit])

      if @debito.debit_type == "recorrente"
        meses = params[:recorrente_meses].to_i
        debitos_criados = 1
        debitos_to_save = []

        if params[:debit].include?(:upload_files_attributes)
          params[:debit].remove(:upload_files_attributes)
        end

        while debitos_criados != meses do
          novo_debito = Debit.new(params[:debit])
          # adiciona mais X meses no vencimento
          novo_debito.due_date = novo_debito.due_date + debitos_criados.month
          # novo_debito.upload_files = nil
          novo_debito.first_debit = @debito

          debitos_to_save << novo_debito
          debitos_criados = debitos_criados + 1
        end

        debitos_to_save.each do |debito|
          debito.save
        end
      elsif @debito.debit_type == "parcelado"
        parcelas = params[:parcelas_numero].to_i
        debitos_criados = 1

        debitos_to_save = []

        while debitos_criados != parcelas do
          novo_debito = Debit.new(params[:debit])
          # novo_debito.upload_files = nil

          novo_debito.due_date = params["data_vencimento_#{debitos_criados}"]
          novo_debito.first_debit = @debito

          debitos_to_save << novo_debito
          debitos_criados = debitos_criados + 1
        end

        debitos_to_save.each do |debito|
          debito.save
        end
      end

      salvou = @debito.save

      if salvou
        redirect_to admin_debito_url(@debito), :notice => "Débito salvo com sucesso."
      else
        render action: "new"
      end
      # super.create
    end
  end
  
  scope :todos, :default => true do |debitos|
    debitos.able_to_view(current_admin_user)
  end

  scope :pendente do |debitos|
    debitos.able_to_view(current_admin_user).where(:status => Debit::STATUS_PENDENTE)
  end

  # somente admin ve conferidos e pagos
  scope :conferido do |debitos|
    if current_admin_user.admin
      debitos.able_to_view(current_admin_user).where(:status => Debit::STATUS_CONFERIDO)
    else
      debitos.able_to_view(current_admin_user).limit(0)
    end
  end
  
  scope :pago do |debitos|
    if current_admin_user.admin
      debitos.where(:status => Debit::STATUS_PAGO)
    else
      debitos.limit(0)
    end
  end

  index do

  	column "COD" do |debito|
      debito.id
    end
    column "Status" do |debito|
      status_tag debito.status, debito.status_tag
    end
    column "Parcela" do |debito|
      debito.parcela_num
    end
    column "Pessoa", :sortable => :description do |debito|
      debito.person.name unless debito.person.blank?
    end
    column "Centro de custo", :sortable => :description do |debito|
      debito.cost_center.name unless debito.cost_center.blank?
    end
    column "Valor", :sortable => :amount do |debito|
      number_to_currency(debito.amount)
    end
    column "Vencimento", :sortable => :due_date do |debito|
      debito.due_date.strftime("%d/%m/%Y") unless debito.due_date.nil?
    end
    column "Fluxo" do |debito|
      if !debito.previous_status_name.blank? and !debito.next_status_name.blank?
        link_to("<< " + debito.previous_status_name, admin_debito_path(debito) + '/previous_status') + " | "  + \
        link_to(debito.next_status_name + " >>", admin_debito_path(debito) + '/next_status')
      elsif debito.previous_status_name.blank? and !debito.next_status_name.blank?
        link_to(debito.next_status_name + " >>", admin_debito_path(debito) + '/next_status')
      else
        link_to("<< " + debito.previous_status_name, admin_debito_path(debito) + '/previous_status')
      end
    end
    column do |debito|
      link_to("Detalhes", admin_debito_path(debito)) + " | " + \
      link_to("Editar", edit_admin_debito_path(debito)) + " | " + \
      link_to("Remover", admin_debito_path(debito), :method => :delete, :confirm => "Tem certeza que deseja excluir?")
    end

  end

  sidebar "Filtrados a Pagar", :only => :index do
    total = debitos.where("status = ? OR status = ?", Debit::STATUS_PENDENTE, Debit::STATUS_CONFERIDO).sum(:amount)
    
    h1 number_to_currency(total), :style => "text-align: center; margin-top: 20px"
  end

  sidebar "Total a Pagar", :only => :index do
    total = Debit.where("status = ? OR status = ?", Debit::STATUS_PENDENTE, Debit::STATUS_CONFERIDO).sum(:amount)
    
    h1 number_to_currency(total), :style => "text-align: center; margin-top: 20px"
  end
  
  filter :person
  filter :cost_center
  filter :core
  filter :due_date
  filter :description


  show :title => :id do
    panel "Detalhes" do
      attributes_table_for debito do
        row("Status") {
          status_tag debito.status, debito.status_tag
        }

        row("Parcela") {
          debito.parcela_num
        }

        row("Vencimento") {
          debito.due_date.strftime("%d/%m/%Y") unless debito.due_date.blank?
        }

        row("Valor") {
          number_to_currency(debito.amount)
        }

        row("Núcleo") {
          debito.core
        }

        row("Centro de custo") {
          debito.cost_center
        }

        row("Pessoa") {
          debito.person
        }

        row("Cliente") {
          debito.client
        }

        row("Descrição") {
          debito.description
        }

        row("Atualizado em") {
          debito.updated_at
        }
      end

      debito.related_debits.each do |deb|
        panel "Débitos relacionados" do
          attributes_table_for deb do
            row("Status") {
              status_tag deb.status, deb.status_tag
            }

            row("Parcela") {
              deb.parcela_num
            }

            row("Vencimento") {
              deb.due_date.strftime("%d/%m/%Y")
            }

            row("Valor") {
              number_to_currency(deb.amount)
            }

            row("Atualizado em") {
              deb.updated_at
            }
          end
        end
      end
      
    end

    panel "Arquivos" do
      table_for debito.upload_files do
        column("Nome do arquivo") do |upload_file|
          if upload_file.name.blank? 
            "<Vazio>"
          else
            upload_file.name
          end
        end

        column("Download") do |upload_file|
          link_to "Download", upload_file.file_ref.url
        end
      end 
    end
  end

  # form(:html => { :multipart => true }) do |f|
  #   f.inputs "Débito" do
  #     f.input :person
  #     f.input :client, collection: Person.customers, as: :select
  #     f.input :cost_center
  #     f.input :core
  #     f.input :amount, as: :string
  #     f.input :due_date, as: :datepicker, style: "width: 180px;"
  #     f.input :status, as: :radio, collection: Debit.status_collection
  #     f.input :description
  #     f.has_many :upload_files do |s|
  #       s.input :name
  #       s.input :file_ref, :as => :file
  #     end
  #   end
  #   f.buttons
  # end
  form :partial => "form"
end
