# encoding: UTF-8
ActiveAdmin.register Income, as: "Receita" do
	menu label: "Contas a Receber"

  collection_action :last_data, :method => :get do
    debito = Income.where("person_id = ?", params[:person_id]).order("id DESC").limit(1)

    render json: debito.first.to_json
  end

  before_filter :only => [:index] do
    if params['commit'].blank?
      # filtro do mes
       params['q'] = {:due_date_gte => Date.today.at_beginning_of_week.strftime("%Y-%m-%d"), :due_date_lte => Date.today.at_end_of_week.strftime("%Y-%m-%d")} 
    end
  end

	controller do
    def next_status
      @income = Income.find(params[:id])
      @income.next_status

      redirect_to :action => :index, :notice => "Status alterado com sucesso."
    end

    def previous_status
      @income = Income.find(params[:id])
      @income.previous_status

      redirect_to :action => :index, :notice => "Status alterado com sucesso."
    end
  end
 	
 	scope :todos, :default => true do |receitas|
 		receitas
 	end

 	scope :notas_a_emitir do |receitas|
   		receitas.where(:status => Income::STATUS_A_EMITIR)
  	end

  	scope :notas_a_receber do |receitas|
    	receitas.where(:status => Income::STATUS_A_RECEBER)
  	end
  
  	scope :recebido do |receitas|
    	receitas.where(:status => Income::STATUS_RECEBIDO)
  	end


  	index do

	  	column "COD" do |receita|
	      receita.id
	    end
	    column "Status" do |receita|
	      status_tag receita.status, receita.status_tag
	    end
      column "Pessoa", :sortable => :description do |receita|
        receita.person.name unless receita.person.blank?
      end
      column "Centro de custo", :sortable => :description do |receita|
        receita.cost_center.name unless receita.cost_center.blank?
      end
	    column "Valor" do |receita|
	      number_to_currency(receita.amount)
	    end
	    column "Vencimento em" do |receita|
	      receita.due_date.strftime("%d/%m/%Y") unless receita.due_date.nil?
	    end
	    column "Fluxo" do |receita|
	      if !receita.previous_status_name.blank? and !receita.next_status_name.blank?
	        link_to("<< " + receita.previous_status_name, admin_receitum_path(receita) + '/previous_status') + " | "  + \
	        link_to(receita.next_status_name + " >>", admin_receitum_path(receita) + '/next_status')
	      elsif receita.previous_status_name.blank? and !receita.next_status_name.blank?
	        link_to(receita.next_status_name + " >>", admin_receitum_path(receita) + '/next_status')
	      else
	        link_to("<< " + receita.previous_status_name, admin_receitum_path(receita) + '/previous_status')
	      end
	    end
	    column do |receita|
	      link_to("Detalhes", admin_receitum_path(receita)) + " | " + \
	      link_to("Editar", edit_admin_receitum_path(receita)) + " | " + \
	      link_to("Remover", admin_receitum_path(receita), :method => :delete, :confirm => "Tem certeza que deseja excluir?")
	    end

  	end

    sidebar "Filtrados a Receber", :only => :index do
      total = receita.where("status = ? OR status = ?", Income::STATUS_A_EMITIR, Income::STATUS_A_RECEBER).sum(:amount)

      h1 number_to_currency(total), :style => "text-align: center; margin-top: 20px"
    end
    sidebar "Total a Receber", :only => :index do 
      total = Income.where("status = ? OR status = ?", Income::STATUS_A_EMITIR, Income::STATUS_A_RECEBER).sum(:amount)

      h1 number_to_currency(total), :style => "text-align: center; margin-top: 20px"
    end
  
  filter :person
  filter :cost_center
  filter :core
  filter :due_date
  filter :description

  show :title => :id do
    panel "Detalhes" do
      attributes_table_for receita do
        row("Status") {
          status_tag receita.status, receita.status_tag
        }

        row("Valor") {
          number_to_currency(receita.amount)
        }

        row("Núcleo") {
          receita.core
        }

        row("Centro de custo") {
          receita.cost_center
        }

        row("Pessoa") {
          receita.person
        }

        row("Cliente") {
          receita.client
        }

        row("Descrição") {
          receita.description
        }

        row("Atualizado em") {
          receita.updated_at
        }
      end
    end

    panel "Arquivos" do
      table_for receita.upload_files do
        column("Nome do arquivo") do |upload_file|
          if upload_file.name.blank? 
            "<Vazio>"
          else
            upload_file.name
          end
        end

        column("Download") do |upload_file|
          link_to "Download", upload_file.file_ref.url
        end
      end 
    end
  end

  form(:html => { :multipart => true }) do |f|
    f.inputs "Receita" do
      f.input :person
      f.input :client, collection: Person.customers, as: :select
      f.input :cost_center
      f.input :core
      f.input :amount, as: :string
      f.input :due_date, as: :datepicker, style: "width: 180px;"
      f.input :status, as: :radio, collection: Income.status_collection
      f.input :description
      f.has_many :upload_files do |s|
        s.input :name
        s.input :file_ref, :as => :file
      end
    end
    f.buttons
  end
end
