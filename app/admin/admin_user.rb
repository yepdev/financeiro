ActiveAdmin.register AdminUser do    

  index do                            
    column :email
    column :admin                     
    column :current_sign_in_at        
    column :last_sign_in_at           
    column :sign_in_count             
    default_actions                   
  end                                

  filter :email                       

  form do |f|                         
    f.inputs "Admin Details" do       
      f.input :email   
      if f.object.new_record?
        f.input :password
        f.input :password_confirmation  
      end
      f.input :admin
      f.input :cost_centers, as: :check_boxes
    end                               
    f.actions                         
  end                                 
end                                   
