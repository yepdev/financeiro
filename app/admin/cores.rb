# encoding: UTF-8
ActiveAdmin.register Core, as: "Nucleo"   do
  menu label: "Núcleos", parent: "Cadastros", priority: 2

  index do
	  	column "COD" do |nucleo|
	      nucleo.id
	    end
	    column "Nome" do |nucleo|
	      nucleo.name
	    end
	    
	    column do |nucleo|
	      link_to("Detalhes", admin_nucleo_path(nucleo)) + " | " + \
	      link_to("Editar", edit_admin_nucleo_path(nucleo)) + " | " + \
	      link_to("Remover", admin_nucleo_path(nucleo), :method => :delete, :confirm => "Tem certeza que deseja excluir?")
	    end

  	end
end
