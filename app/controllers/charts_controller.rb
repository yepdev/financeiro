# encoding: UTF-8
class ChartsController < ApplicationController
  def debits



  	data_table = GoogleVisualr::DataTable.new
  	data_table.new_column('string', 'Mes' ) 
	data_table.new_column('number', 'TI') 
	data_table.new_column('number', 'VIVO')

	data_table.add_rows([ 
	  ['Janeiro', 1000, 400], 
	  ['Fevereiro', 1170, 460], 
	  ['Março', 660, 1120], 
	  ['Abril', 1030, 540] 
	])

	option = { width: '500', height: '300', title: 'Débitos por Núcleo' }
	@chart = GoogleVisualr::Interactive::AreaChart.new(data_table, option)


  end

  def incomes

  	data_table = GoogleVisualr::DataTable.new
	data_table.new_column('string', 'Mês')
	data_table.new_column('number', 'Valor')
	data_table.add_rows(4)
	data_table.set_cell(0, 0, 'Janeiro'     )
	data_table.set_cell(0, 1, 6 )
	data_table.set_cell(1, 0, 'Fevereiro'      )
	data_table.set_cell(1, 1, 3  )
	data_table.set_cell(2, 0, 'Março'  )
	data_table.set_cell(2, 1, 4  )
	data_table.set_cell(3, 0, 'Abril' )
	data_table.set_cell(3, 1, 0.95  )
	 
	  opts   = { :width => 500, :height => 300, :title => 'Receitas', :is3D => true }
	  @chart = GoogleVisualr::Interactive::PieChart.new(data_table, opts)
  end
end
