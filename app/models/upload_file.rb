class UploadFile < ActiveRecord::Base
  attr_accessible :debit_id, :file_ref, :income_id, :name

  belongs_to :debit, :polymorphic => true
  belongs_to :income, :polymorphic => true

  mount_uploader :file_ref, FileUploader
end
