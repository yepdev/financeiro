class CostCenter < ActiveRecord::Base
  attr_accessible :name
  has_many :debits

  has_many :cost_center_view
  has_many :admin_users, through: :cost_center_view
end
