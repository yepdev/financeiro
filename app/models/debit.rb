# encoding: UTF-8
class Debit < ActiveRecord::Base
  STATUS_PENDENTE = 'pendente'
  STATUS_CONFERIDO  = 'conferido'
  STATUS_PAGO  = 'pago'

  TIPO_FIXO = 'fixo'
  TIPO_RECORRENTE = 'recorrente'
  TIPO_PARCELADO = 'parcelado'

  attr_accessible :amount, :core_id, :cost_center_id, :description, :due_date, :person_id, :status, :client_id, :upload_files_attributes, :upload_files, :debit_type, :first_debit_id
  belongs_to :cost_center
  belongs_to :core
  belongs_to :person , :foreign_key => :person_id, :class_name => "Person"
  belongs_to :client, :foreign_key => :client_id, :class_name => "Person"
  # belongs_to :client

  validates :status, :inclusion => { :in => [STATUS_PENDENTE, STATUS_CONFERIDO, STATUS_PAGO], :message => "Você deve selecionar um status." }

  has_many :upload_files
  accepts_nested_attributes_for :upload_files

  belongs_to :first_debit,
      foreign_key: "first_debit_id",
      class_name: "Debit"

  has_many :child_debits,
      foreign_key: "first_debit_id",
      class_name: "Debit"

  def self.able_to_view(current_user)
    return limit(0) if current_user.blank?

    if current_user.admin
      where("")
    elsif !current_user.cost_centers.blank?
      where("cost_center_id IN ( ? )", current_user.cost_centers)
    else
      limit(0)
    end
  end

  def able_to_view(current_user)
    return limit(0) if current_user.blank?

    if current_user.admin
      where("")
    elsif !current_user.cost_centers.blank?
      where("cost_center_id IN ( ? )", current_user.cost_centers)
    else
      limit(0)
    end
  end

  def related_debits
    if self.child_debits.any?
      self.child_debits.order("due_date asc")
    elsif !self.first_debit_id.blank? and self.first_debit.class != NilClass
      parcelas = []
      parcelas << self.first_debit
      parcelas = parcelas + self.first_debit.child_debits.where("id <> ?", self.id).order("due_date asc")

      return parcelas
    else
      []
    end
  end

  def parcela_num
    if self.child_debits.any?
      parcelas = self.child_debits.order("due_date asc")

      return "1/#{parcelas.size + 1}"
    elsif !self.first_debit_id.blank? and self.first_debit.class != NilClass
      parcelas = []
      parcelas << self.first_debit
      
      parcelas = parcelas + self.first_debit.child_debits.order("due_date asc")

      qual_parcela = parcelas.index(self)
      return "#{qual_parcela + 1}/#{parcelas.size}"
    else
      return "-"
    end

  end

  def status_tag
    case self.status
      when STATUS_PENDENTE then :error
      when STATUS_CONFERIDO then :warning
      when STATUS_PAGO then :ok
    end
  end

  def self.status_collection
    {
      "Pendente" => STATUS_PENDENTE,
      "Conferido" => STATUS_CONFERIDO,
      "Pago" => STATUS_PAGO
    }
  end

  def self.tipo_collection
    {
      "Fixo" => TIPO_FIXO,
      "Recorrente" => TIPO_RECORRENTE,
      "Parcelado" => TIPO_PARCELADO
    }
  end

  def previous_status
    if self.status.eql? STATUS_CONFERIDO
      self.status = STATUS_PENDENTE
      self.save
    elsif self.status.eql? STATUS_PAGO
      self.status = STATUS_CONFERIDO
      self.save
    end
  end

  def next_status
    if self.status.eql? STATUS_CONFERIDO
      self.status = STATUS_PAGO
      self.save
    elsif self.status.eql? STATUS_PENDENTE
      self.status = STATUS_CONFERIDO
      self.save
    end
  end

  def next_status_name
    if self.status.eql? STATUS_CONFERIDO
      return "Pago"
    elsif self.status.eql? STATUS_PENDENTE
      return "Conferido"
    else
      return nil
    end
  end

  def previous_status_name
    if self.status.eql? STATUS_CONFERIDO
      return "Pendente"
    elsif self.status.eql? STATUS_PAGO
      return "Conferido"
    else
      return ""
    end
  end

  def next_status_name
    if self.status.eql? STATUS_CONFERIDO
      return "Pago"
    elsif self.status.eql? STATUS_PENDENTE
      return "Conferido"
    else
      return ""
    end
  end

end
