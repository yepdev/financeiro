class BankData < ActiveRecord::Base
  attr_accessible :account_number, :agency, :bank_number, :operation_code, :person_id

  belongs_to :person
end
