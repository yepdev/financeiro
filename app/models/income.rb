# encoding: UTF-8
class Income < ActiveRecord::Base
  
  STATUS_A_EMITIR = 'notas_a_emitir'
  STATUS_A_RECEBER  = 'notas_a_receber'
  STATUS_RECEBIDO  = 'recebido'

  attr_accessible :amount, :client_id, :core_id, :cost_center_id, :description, :due_date, :person_id, :status, :upload_files_attributes, :upload_files

  belongs_to :person , :foreign_key => :person_id, :class_name => "Person"
  belongs_to :client, :foreign_key => :client_id, :class_name => "Person"
  belongs_to :cost_center
  belongs_to :core

  validates :status, :inclusion => { :in => [STATUS_A_EMITIR, STATUS_A_RECEBER, STATUS_RECEBIDO], :message => "Você deve selecionar um status." }

  has_many :upload_files
  accepts_nested_attributes_for :upload_files

  def self.able_to_view(current_user)
    return limit(0) if current_user.blank?

    if current_user.admin
      where("")
    elsif !current_user.cost_centers.blank?
      where("cost_center_id IN ( ? )", current_user.cost_centers)
    else
      limit(0)
    end
  end

  def able_to_view(current_user)
    return limit(0) if current_user.blank?

    if current_user.admin
      where("")
    elsif !current_user.cost_centers.blank?
      where("cost_center_id IN ( ? )", current_user.cost_centers)
    else
      limit(0)
    end
  end

  def status_tag
    case self.status
      when STATUS_A_EMITIR then :error
      when STATUS_A_RECEBER then :warning
      when STATUS_RECEBIDO then :ok
    end
  end

  def self.status_collection
    {
      "Notas a Emitir" => STATUS_A_EMITIR,
      "Notas a Receber" => STATUS_A_RECEBER,
      "Recebido" => STATUS_RECEBIDO
    }
  end

   def previous_status
    if self.status.eql? STATUS_A_RECEBER
      self.status = STATUS_A_EMITIR
      self.save
    elsif self.status.eql? STATUS_RECEBIDO
      self.status = STATUS_A_RECEBER
      self.save
    end
  end

  def next_status
    if self.status.eql? STATUS_A_RECEBER
      self.status = STATUS_RECEBIDO
      self.save
    elsif self.status.eql? STATUS_A_EMITIR
      self.status = STATUS_A_RECEBER
      self.save
    end
  end

  def next_status_name
    if self.status.eql? STATUS_A_RECEBER
      return "Recebido"
    elsif self.status.eql? STATUS_A_EMITIR
      return "A Receber"
    else
      return nil
    end
  end

  def previous_status_name
    if self.status.eql? STATUS_A_RECEBER
      return "A Emitir"
    elsif self.status.eql? STATUS_RECEBIDO
      return "A Receber"
    else
      return ""
    end
  end

  def next_status_name
    if self.status.eql? STATUS_A_RECEBER
      return "Recebido"
    elsif self.status.eql? STATUS_A_EMITIR
      return "A Receber"
    else
      return ""
    end
  end
  
end
