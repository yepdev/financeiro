class Core < ActiveRecord::Base
  attr_accessible :description, :name
  has_many :debits
end
