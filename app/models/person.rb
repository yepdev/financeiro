class Person < ActiveRecord::Base
  attr_accessible :addrress, :cellphone, :company, :email, :name, :phone, :state_register, :municipal_register, :enrollment, :activity_area, :register, :voter_register, :cpf_cnpj, :bank_data, :bank_data_attributes, :person_category_id
  has_many :debits
  has_one :bank_data
  has_many :incomes
  belongs_to :person_category

  FUNCIONARIO = 1
  CLIENTE = 2
  OUTROS = 3
  FORNECEDOR = 4
  ADVOGADO = 5
  FISCO = 6

  accepts_nested_attributes_for :bank_data

  def self.customers
  	where("person_category_id = ?", CLIENTE).order(:name)
  end
end
