class CostCenterView < ActiveRecord::Base
  attr_accessible :admin_user_id, :cost_center_id

  belongs_to :admin_user
  belongs_to :cost_center
end
