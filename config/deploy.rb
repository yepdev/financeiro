set :application, "financeiro"
set :repository,  "ssh://git@bitbucket.org/yepdev/financeiro.git"

set :scm, :git

set :deploy_to , "/home/suporte/projetos/#{application}"

#user to login to server via ssh
set :user, "suporte"

#do not use sudo
set :use_sudo, false

role :web, "192.168.1.2"                          # Your HTTP server, Apache/etc
role :app, "192.168.1.2"                          # This may be the same as your `Web` server
role :db,  "192.168.1.2", :primary => true # This is where Rails migrations will run

#Deploy on suburl
set :asset_env, "#{asset_env} RAILS_RELATIVE_URL_ROOT=/#{application}"

# Load RVM's capistrano plugin.
require "rvm/capistrano"
set :rvm_path, "$HOME/.rvm"
set :rvm_type, :user  # Don't use system-wide RVM

#bundler config
set :bundle_roles, [:app]
require 'bundler/capistrano'


namespace :deploy do
  desc "reload the database with seed data"
  task :seed do
    run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end
end

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end

end

